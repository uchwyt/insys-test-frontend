/**
 * Created by marcinek on 23.03.19.
 * template true function
 */

import React from 'react';
import styles from './Profile.module.css';
import PlaceIcon from '../components/icons/Place';

const Profile = () => {
  return (
    <article className='p-4'>
      <h1 className={styles.header}>Marilyn Monroe</h1>
      <h3 className={styles.subheader}><PlaceIcon /> Poznań, PL</h3>

      <p>
 Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum nec mi neque. Suspendisse rhoncus ligula non libero interdum faucibus. Donec vitae dolor vitae lacus lobortis ultrices a vel felis. Cras velit nulla, pretium id commodo at, auctor eu nisi. Maecenas cursus quam orci, non congue eros mollis ac.
      </p>
      <blockquote className={styles.blockquote}>
        Integer ut aliquam sapien, ac ultricies felis. Curabitur nec enim enim. Nunc at dapibus ante. Nullam gravida, arcu at varius fringilla, elit velit
        mattis massa, ac vehicula augue sem non nunc. Etiam pellentesque vel mauris ac malesuada. Sed commodo diam quam, vitae aliquam nisl ultrices quis. Etiam
        congue augue id cursus ultrices. Ut dapibus et lacus eu egestas.
      </blockquote>
      <p>
 Maecenas vitae turpis et diam gravida venenatis a vitae neque. Sed lacinia magna sit amet nunc tempor pellentesque. Donec ut fermentum massa, eu imperdiet nibh. Nunc et orci posuere, efficitur sem non, sodales mauris. Suspendisse in faucibus risus, sit amet porttitor odio.
      </p>
    </article>
  );
};

export default Profile;
