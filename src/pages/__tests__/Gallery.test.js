import React from 'react';
import { render, cleanup } from 'react-testing-library';

import {Gallery} from '../Gallery';

afterEach(cleanup);

describe('Gallery', () => {
  const dispatch = jest.fn();

  it('renders with no gallery and loader', () => {
    'use strict';
    const {container} = render(<Gallery dispatch={dispatch}/>);
    expect(container).toMatchSnapshot();
  });

  it('renders with loader', () => {
    'use strict';
    const {container} = render(<Gallery isLoading dispatch={dispatch}/>);
    expect(container).toMatchSnapshot();
  });

  it('renders with gallery', () => {
    'use strict';
    const list = [
      {
        title: 'title',
        media: {m: 'a'},
        link: 'https://example.com',
      }
    ];

    const {container} = render(<Gallery list={list} dispatch={dispatch}/>);
    expect(container).toMatchSnapshot();
  });

  it('renders only 9 images', () => {
    'use strict';
    const list = [
      {
        title: 'title 1',
        media: {m: 'a'},
        link: 'https://example.com',
      },
      {
        title: 'title 2',
        media: {m: 'a'},
        link: 'https://example.com',
      },
      {
        title: 'title 3',
        media: {m: 'a'},
        link: 'https://example.com',
      },
      {
        title: 'title 4',
        media: {m: 'a'},
        link: 'https://example.com',
      },
      {
        title: 'title 5',
        media: {m: 'a'},
        link: 'https://example.com',
      },
      {
        title: 'title 6',
        media: {m: 'a'},
        link: 'https://example.com',
      },
      {
        title: 'title 7',
        media: {m: 'a'},
        link: 'https://example.com',
      },
      {
        title: 'title 8',
        media: {m: 'a'},
        link: 'https://example.com',
      },
      {
        title: 'title 9',
        media: {m: 'a'},
        link: 'https://example.com',
      },
      {
        title: 'title 10',
        media: {m: 'a'},
        link: 'https://example.com',
      },
    ];

    const {container} = render(<Gallery list={list} dispatch={dispatch}/>);
    expect(container).toMatchSnapshot();
  });
});
