import React from 'react';
import { render, cleanup } from 'react-testing-library';

import Profile from '../Profile';

afterEach(cleanup);

describe('Profile', () => {
  it('renders', () => {
    'use strict';
    const {container} = render(<Profile/>);
    expect(container).toMatchSnapshot();
  });
});
