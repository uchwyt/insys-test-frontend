/**
 * Created by marcinek on 23.03.19.
 * template true function
 */

import React from 'react';
import { connect } from 'react-redux';
import { fetchImages } from '../redux/actions';
import Loader from '../components/Loader';
import GalleryItem from '../components/GalleryItem';
import PropTypes from 'prop-types';
import { Row } from 'reactstrap';

export const Gallery = ({dispatch, list, isLoading}) => {
  React.useEffect(() => {
    dispatch(fetchImages({
      tags: 'marilynmonroe',
    }));
  }, []);
  const items = Array.isArray(list) ? list.splice(0, 9) : [];

  return (
    <Loader isLoading={isLoading}>
      <Row className='p-4'>
        {
          items.map((a, i) => <GalleryItem key={i} {...a} />)
        }
      </Row>
    </Loader>
  );
};

Gallery.propTypes = {
  dispatch: PropTypes.func.isRequired,
  list: PropTypes.array,
  isLoading: PropTypes.bool,
};

const selector = state => ({
  list: state.gallery.all,
  isLoading: state.gallery.isFetching,
});

export default connect(selector)(Gallery);
