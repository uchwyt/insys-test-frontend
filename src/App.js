import React from 'react';
import Routes from './Routes';
import { Provider } from 'react-redux';
import { store } from './redux/redux';
import { BrowserRouter as Router} from 'react-router-dom';

const App = () => {
  return (
    <div className="App">
      <Provider store={store} className="App">
        <Router>
          <Routes/>
        </Router>
      </Provider>
    </div>
  )
};

export default App;
