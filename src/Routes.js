import React from 'react';
import { Route, Switch } from 'react-router-dom';
import PageContainer from './components/PageContainer';
import Gallery from './pages/Gallery';
import Profile from './pages/Profile';

const Routes = () => {
  return (
    <PageContainer>
      <Switch>
        <Route path='/gallery' exact component={Gallery}/>
        <Route path='/profile' exact component={Profile} />
      </Switch>
    </PageContainer>
  )
};

export default Routes;
