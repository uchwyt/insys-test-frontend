import React from 'react';
import { render, cleanup } from 'react-testing-library';

import Avatar from '../Avatar';

afterEach(cleanup);

describe('Avatar', () => {
  it('render with no children', () => {
    'use strict';
    const {container} = render(<Avatar/>);
    expect(container).toMatchSnapshot();
  });
  it('render with image - src', () => {
    'use strict';
    const {container} = render(<Avatar src='https://example.com/images/image.jpg' alt='alternative text'/>);
    expect(container).toMatchSnapshot();
  });
  it('render with image - srcSet ', () => {
    'use strict';
    const {container} = render(<Avatar srcSet='https://example.com/images/image.jpg 1200w' sizes='100%'/>);
    expect(container).toMatchSnapshot();
  });
  it('render with custom children', () => {
    'use strict';
    const {container} = render(<Avatar><p>Lorem ipsum</p></Avatar>);
    expect(container).toMatchSnapshot();
  });
});
