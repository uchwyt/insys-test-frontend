import React from 'react';
import { render, cleanup } from 'react-testing-library';

import Loader from '../Loader';

afterEach(cleanup);

describe('Loader', () => {
  it('render with no children', () => {
    'use strict';
    const {container} = render(<Loader/>);
    expect(container).toMatchSnapshot();
  });
  it('render with children', () => {
    'use strict';
    const {container} = render(<Loader><p>test</p></Loader>);
    expect(container).toMatchSnapshot();
  });
  it('render loader', () => {
    'use strict';
    const {container} = render(<Loader isLoading><p>test</p></Loader>);
    expect(container).toMatchSnapshot();
  });
});
