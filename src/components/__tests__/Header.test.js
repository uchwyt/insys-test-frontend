import React from 'react';
import { cleanup } from 'react-testing-library';
import { renderWithRouter } from '../../testHelpers';

import Header from '../Header';

afterEach(cleanup);

describe('Header', () => {
  it('should render header', () => {
    'use strict';
    const {container} = renderWithRouter(<Header/>);
    expect(container).toMatchSnapshot();
  });

  it('should render header - with title attribute on header element', () => {
    'use strict';
    const {container} = renderWithRouter(<Header title='title'/>);
    expect(container).toMatchSnapshot();
  });
});
