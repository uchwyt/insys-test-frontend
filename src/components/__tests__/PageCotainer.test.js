import React from 'react';
import { cleanup } from 'react-testing-library';
import { renderWithRouter } from '../../testHelpers';
import PageContainer from '../PageContainer';

afterEach(cleanup);

describe('PageContainer', () => {
  it('should render with no children', () => {
    'use strict';
    const {container} = renderWithRouter(<PageContainer/>);
    expect(container).toMatchSnapshot();
  });

  it('should render with children elements', () => {
    'use strict';
    const {container} = renderWithRouter(<PageContainer><p>1</p><p>2</p></PageContainer>);
    expect(container).toMatchSnapshot();
  });
});
