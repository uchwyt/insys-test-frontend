import React from 'react';
import { render, cleanup } from 'react-testing-library';

import ErrorBoundary from '../ErrorBoundary';

afterEach(cleanup);

describe('ErrorBoundary', () => {
  const ComponentWithError = () => { throw new Error()};

  it('should render children', () => {
    'use strict';
    const {container} = render(<ErrorBoundary><p>test</p></ErrorBoundary>);
    expect(container).toMatchSnapshot();
  });
  it('should render error message', () => {
    'use strict';
    const {container} = render(<ErrorBoundary><ComponentWithError /></ErrorBoundary>);
    expect(container).toThrowErrorMatchingSnapshot();
  });
});
