import React from 'react';
import { render, cleanup } from 'react-testing-library';

import GalleryItem from '../GalleryItem';

afterEach(cleanup);

describe('GalleryItem', () => {
  it('renders w/o image', () => {
    'use strict';
    const {container} = render(<GalleryItem link='www.a.pl' title='title' media={'a'} />);

    expect(container).toMatchSnapshot();
  });

  it('renders with image', () => {
    'use strict';
    const {container} = render(<GalleryItem link='www.a.pl' title='title' media={{m: 'https://farm5.staticflickr.com/4069/4294340605_d3c49c23a3_n.jpg'}} />);
    expect(container).toMatchSnapshot();
  });
});
