/**
 * Created by marcinek on 21.02.18.
 * template react container
 */

import React  from 'react';
import PropTypes from 'prop-types';

class ErrorBoundary extends React.Component {
  constructor(props) {
    super(props);
    this.state = {hasError: false};
  }

  componentDidCatch(error, info) {
    // Display fallback UI
    this.setState({hasError: true});
    // You can also log the error to an error reporting service
    console.log(error.toString());
    console.log(info);
  }

  render() {
    return (
      <>
        {
          this.state.hasError ?
          <>
            <h5>Something went wrong.</h5>
            <p>
              Please go back to <a href="/">home page</a> or try to refresh.
            </p>
          </>
          :
          this.props.children
        }
      </>
    );
  }
}


ErrorBoundary.propTypes = {
  children: PropTypes.node,
};

export default ErrorBoundary;
