/**
 * Created by marcinek on 23.03.19.
 * template true function
 */

import React from 'react';
import PropTypes from 'prop-types';
import { Card, CardImg, CardImgOverlay, CardTitle, Col } from 'reactstrap';
import styles from './GalleryItem.module.css';

const GalleryItem = ({title, media, link}) => {

  return (
    typeof (media) === 'object' && media.m &&
    <Col xs={12} md={6} xl={4} className={styles.item}>
      <Card>
        <a href={link} target='_blank' rel='noopener noreferrer'>
          <CardImg className={styles.image} top width='100%' src={media.m}/>
          <CardImgOverlay className={styles.overlay}>
            <CardTitle className={styles.title}>{title}</CardTitle>
          </CardImgOverlay>
        </a>
      </Card>
    </Col>
  );
};

GalleryItem.propTypes = {
  title: PropTypes.node.isRequired,
  media: PropTypes.shape({
    m: PropTypes.string,
  }).isRequired,
  link: PropTypes.string.isRequired,
};

export default GalleryItem;
