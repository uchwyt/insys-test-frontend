import React from 'react';
import PropTypes from 'prop-types';
import {Row, Col, Container} from 'reactstrap';
import ErrorBoundary from './ErrorBoundary';
import Helmet from 'react-helmet';
import Header from './Header';

const PageContainer = ({children}) => {
  return (
    <Container>
      <ErrorBoundary>
        <Helmet
          defaultTitle="insys-test-app"
          titleTemplate="%s - test app"
        />
        <Header/>
        <Row>
          <Col xs={12}>
            {children}
          </Col>
        </Row>
      </ErrorBoundary>
    </Container>
  )
};

Container.propTypes = {
  children: PropTypes.node,
};

export default PageContainer;
