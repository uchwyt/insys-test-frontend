/**
 * Created by marcinek on 23.03.19.
 * template true function
 */

import React from 'react';
import PropTypes from 'prop-types';
import styles from './Avatar.module.css';
import cx from 'clsx';

const Avatar = ({alt, children: childrenProp, sizes, src, srcSet, className, ...rest}) => {
  const img = src || srcSet;
  let children = null;
  if(img) {
    children = (
      <img
        alt={alt}
        src={src}
        srcSet={srcSet}
        sizes={sizes}
        className={styles.img} />
    )
  } else {
    children = childrenProp;
  }

  return (
    <figure className={cx(styles.root, className)} {...rest}>
      {children}
    </figure>
  );
};

Avatar.propTypes = {
  alt: PropTypes.string,
  children: PropTypes.node,
  sizes: PropTypes.string,
  src: PropTypes.string,
  srcSet: PropTypes.string,
};

export default Avatar;
