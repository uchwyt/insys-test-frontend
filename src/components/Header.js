/**
 * Created by marcinek on 23.03.19.
 * template true function
 */

import React from 'react';
import { NavLink as Link } from 'react-router-dom';
import {Navbar, NavbarBrand, Nav, NavLink, NavItem} from 'reactstrap';
import ImageIcon from './icons/Image';
import AccountIcon from './icons/Account';
import Avatar from './Avatar';
import styles from './Header.module.css';

const Header = () => {
  return (
    <header>
      <img className={styles.image} src='https://www.funclub.pl/wp-content/themes/twentyseventeen/assets/images/header.jpg' alt='Header'/>
      <Navbar light className={styles.navbar} expand>
        <NavbarBrand>
          <Avatar className={styles.avatar} src='https://farm7.staticflickr.com/6188/6062051035_ebbc3cf7b1_b.jpg' alt='Marilyn Monroe' />
        </NavbarBrand>
          <Nav className='ml-auto' navbar horizontal='end'>
            <NavItem>
              <NavLink tag={Link} activeClassName={styles.active} to='/profile'>
                <AccountIcon className={styles.icon} />
              </NavLink>
            </NavItem>
            <NavItem>
              <NavLink tag={Link} activeClassName={styles.active} to='/gallery'>
                <ImageIcon className={styles.icon} />
              </NavLink>
            </NavItem>
          </Nav>
      </Navbar>
    </header>
  );
};

export default Header;
