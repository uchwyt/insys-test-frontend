/**
 * Created by marcinek on 23.01.19.
 * template true function
 */

import React from 'react';
import PropTypes from 'prop-types';
import {Spinner} from 'reactstrap';

const Loader = ({isLoading, children}) => (
  isLoading ?
  <Spinner color='primary'/>
            :
  <>{children}</>
);

Loader.propTypes = {
  isLoading: PropTypes.bool.isRequired,
  children: PropTypes.node,
};

Loader.defaultProps = {
  isLoading: false,
};

export default Loader;
