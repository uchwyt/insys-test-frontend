import React from 'react';
import {render} from 'react-testing-library';
import { BrowserRouter as Router} from 'react-router-dom';

export const renderWithRouter = (children) => {
  return render(<Router>{children}</Router>);
};
