import React from 'react';
import { cleanup } from 'react-testing-library';

import {generateActions, createSearch} from '../helpers';

afterEach(cleanup);

describe('generateActions', () => {
  "use strict";
  const data = [
    'GALLERY',
  ];

  it('return 6 keys', () => {
    const expected = [
      'REQUEST_GALLERY',
      'RECEIVE_GALLERY',
      'ERROR_GALLERY',
    ];

    expect(Object.keys(generateActions(data))).toEqual(expected);
  });

  it('return empty object', () => {
    expect(Object.keys(generateActions('GALLERY'))).toEqual([]);
  });
});


describe('createSearch', () => {
  "use strict";

  it('return proper query string', () => {
    const data = {
      'a': 'b',
      'c': 'd',
    };
    const expected = '?a=b&c=d';

    expect(createSearch(data)).toEqual(expected);
  });

  it('return empty string', () => {
    expect(createSearch('a')).toEqual('');
  });

  it('convert array values to query string', () => {
    const data = [
      'b',
      'd',
    ];

    expect(createSearch(data)).toEqual('?0=b&1=d');
  });
});
