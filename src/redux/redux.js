import { createStore, combineReducers, applyMiddleware } from 'redux';
import reducers from './reducers';
import thunk from 'redux-thunk';

export const store = applyMiddleware(thunk)(createStore)(combineReducers(reducers));
