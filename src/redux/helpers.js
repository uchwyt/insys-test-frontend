
/**
 * Generates standard actions for state. Request, Receive and Error.
 */
export function generateActions(list) {
  let tmp = {};
  if (Array.isArray(list)) {
    const func = (data) => ({data});
    list.forEach(key => {
      tmp[`REQUEST_${key}`] = func;
      tmp[`RECEIVE_${key}`] = func;
      tmp[`ERROR_${key}`] = func;
    })
  }
  return tmp;
}

export function createSearch(data) {
  let ret = [];
  if(typeof(data) === 'object') {
    Object.keys(data).forEach(key => {
      const one = data[key];
      if (key !== '' && one && one !== "") {
        ret.push(`${key}=${encodeURIComponent(one)}`);
      }
    });
    return `?${ret.join("&")}`;
  }
  return '';
}

export const createReducers = (list, Constants) => {
  let reducers = {};
  Object.keys(list).forEach(key => {
    const {def, actions} = list[key];
    reducers[key] = (state = def, action) => {
      const types = {
        [Constants[`REQUEST_${key.toUpperCase()}`]]: state => setData(state, def, {isFetching: true}),
        [Constants[`RECEIVE_${key.toUpperCase()}`]]: (state, action) => setData(state, def, {all: action.data}),
        [Constants[`ERROR_${key.toUpperCase()}`]]: state => setData(state, def, {error: true}),
        ...actions
      };
      return types[action.type] ? types[action.type](state, action) : state;
    };
  });
  return reducers;
};

export const setData = (state, def, data) => Object.assign({}, state, def, data);
