import {createReducers} from './helpers';
import {Constants} from './actions';

const reducers = createReducers({
  gallery: {
    def: {
      all: [],
      error: false,
      isFetching: false,
    }
  }
}, Constants);

export default reducers;
