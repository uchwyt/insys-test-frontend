import EasyActions from 'redux-easy-actions';
import { createSearch, generateActions } from './helpers';
import axios from 'axios';
import jsonpAdapter from 'axios-jsonp';
import es6 from 'es6-promise';

es6.polyfill();

const states = [
  'GALLERY'
];

const stateList = generateActions(states);

const {Actions, Constants} = EasyActions(stateList);

export {Actions, Constants};

export function fetchImages(data) {
  return dispatch => {
    dispatch(Actions.REQUEST_GALLERY());

    const cb = res => {
      if(parseInt(res.status) === 200 && res.data) {
        dispatch(Actions.RECEIVE_GALLERY(res.data.items));
      } else {
        dispatch(Actions.ERROR_GALLERY());
      }
      return Promise.resolve();
    };

    return axios({
      url: 'https://api.flickr.com/services/feeds/photos_public.gne' + createSearch({
        format: 'json',
        ...data,
      }),
      adapter: jsonpAdapter,
      callbackParamName: 'jsoncallback',
    }).then(cb);
  }
};
